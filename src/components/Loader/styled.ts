import styled, { keyframes } from "styled-components/macro";
import { theme } from "theme";

const animation = keyframes`
  0%, 80%, 100% {
    opacity: .5;
  } 
  40% {
       opacity: 1;
  }
`;

export const ScLoader = styled.div`
  position: fixed;
  left: calc(50% - 21px);
  top: 50%;
  font-size: 18px;
  transform: translate(-50%, -50%);

  span {
    display: inline-block;
    width: 4px;
    height: 4px;
    background-color: ${theme.color.white};
    margin-right: 3px;
    opacity: 0.5;
    border-radius: 50%;
    animation: ${animation} 1.4s infinite ease-in-out both;
    animation-delay: 0s;
    &:nth-child(3) {
      margin-right: 6px;
      animation-delay: -0.32s;
    }
    &:nth-child(4) {
      animation-delay: -0.16s;
    }
  }
`;
