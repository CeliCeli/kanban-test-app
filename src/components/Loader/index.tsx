import React, { FC } from "react";
import { ScLoader } from "./styled";

export const Loader: FC = () => {
  return (
    <ScLoader>
      <span />
      <span />
      <span />
      Loading
    </ScLoader>
  );
};
