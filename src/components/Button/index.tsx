import React, { FC } from "react";

import { ButtonProps } from "models/button";
import { ScButton } from "./styled";

export const Button: FC<ButtonProps> = ({
  children,
  color,
  onClick,
  className,
  disabled,
  id,
}) => {
  return (
    <ScButton
      disabled={disabled}
      onClick={onClick}
      color={color}
      className={className}
      id={id}
    >
      {children}
    </ScButton>
  );
};
