import styled, { css } from "styled-components/macro";
import { theme } from "theme";
import { ButtonTheme, ScButtonProps } from "models/button";
import { hexToRGB } from "helpers/hexToRgb";

export const ScButton = styled.button<ScButtonProps>`
  background: transparent;
  border: none;
  cursor: pointer;
  font-size: 14px;
  color: ${theme.color.white};
  padding: 8px 16px;
  border-radius: 8px;
  font-weight: bold;
  transition: 0.2s;

  ${({ color }) => {
    switch (color) {
      case ButtonTheme.Secondary:
        return css`
          /* some css there  :) */
        `;

      default:
        return css`
          background-color: ${theme.console.primary};
          color: ${hexToRGB(theme.color.white, 0.5)};
          &:hover {
            color: ${hexToRGB(theme.color.white, 1)};
          }
        `;
    }
  }}
`;
