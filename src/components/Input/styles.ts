import styled, { css } from "styled-components/macro";
import { theme } from "theme";
import { ScInputProps } from "models/input";
import { hexToRGB } from "helpers/hexToRgb";

export const ScInput = styled.div<ScInputProps>`
  width: 100%;
  position: relative;
  display: flex;
  max-width: 380px;

  &:not(:last-child) {
    margin-bottom: 24px;
  }

  label {
    display: block;
    margin-bottom: 4px;
  }

  input,
  textarea {
    height: 38px;
    border-radius: ${({ withButton }) => (withButton ? "8px 0 0 8px" : "8px")};
    background-color: ${theme.console.primary};
    color: ${theme.color.white};
    outline: none;
    width: 100%;
    padding: ${({ withIcon, withButton }) =>
      `0 ${withButton ? "48px" : "16px"}  0 ${withIcon ? "48px" : "16px"} `};
    box-sizing: border-box;
    font-size: 14px;
    transition: 0.3s;
    border: 2px solid ${theme.console.primary};

    &::placeholder {
      color: ${hexToRGB(theme.console.white, 0.1)};
    }

    &:focus {
      border: 2px solid ${theme.console.primary};
      background-color: ${theme.console.black};
    }

    &[readonly] {
      cursor: default;
      background: ${theme.color.secondaryLight};
    }
  }

  & > svg {
  }

  & > button {
    border-radius: 0 8px 8px 0;
    position: relative;
    &:before {
      position: absolute;
      content: "";
      left: -4px;
      width: 2px;
      top: 8px;
      bottom: 8px;
      background-color: ${theme.console.black};
    }
  }

  ${({ isFilled }) =>
    css`
      & > svg {
        position: absolute;
        bottom: 16px;
        left: 16px;
        filter: grayscale(${isFilled ? "0" : "1"});
        opacity: ${isFilled ? "1" : "0.7"};
        transition: 0.2s;
      }
    `}
`;
