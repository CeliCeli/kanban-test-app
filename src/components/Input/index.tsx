import React, { FC, useMemo } from "react";
import { v4 as uuid } from "uuid";
import { InputProps, InputTypes } from "models/input";
import { ScInput } from "./styles";

export const Input: FC<InputProps> = ({
  label,
  type = InputTypes.Text,
  placeholder,
  value,
  isError,
  withIcon,
  withButton,
  readonly,
  width,
  height,
  className,
  id: tagId,
  dataKey,
  onChange,
  onKeyUp,
  onKeyDown,
  onBlur,
  onFocus,
}) => {
  const id = uuid();

  const getIsFilled = useMemo(() => {
    if (!value) return;
    return value.toString().length > 0;
  }, [value]);

  const isTextarea: boolean = useMemo(() => {
    switch (type) {
      case InputTypes.Textarea:
        return true;
      default:
        return false;
    }
  }, [type]);

  const elId = tagId ?? id;

  return (
    <ScInput
      withIcon={withIcon}
      withButton={withButton}
      isFilled={getIsFilled}
      isError={isError}
      width={width}
      height={height}
      className={className}
    >
      {label && <label htmlFor={elId + "id"}>{label}</label>}
      {withIcon}
      {!isTextarea ? (
        <input
          type={type}
          placeholder={placeholder}
          value={value}
          name={elId + "id"}
          id={elId + "id"}
          onChange={onChange}
          onKeyUp={onKeyUp}
          onKeyDown={onKeyDown}
          onBlur={onBlur}
          readOnly={readonly}
          data-key={dataKey}
          onFocus={onFocus}
        />
      ) : (
        <textarea
          placeholder={placeholder}
          name={elId + "id"}
          id={elId + "id"}
          onChange={onChange}
          onKeyUp={onKeyUp}
          onKeyDown={onKeyDown}
          onBlur={onBlur}
          value={value}
          readOnly={readonly}
          data-key={dataKey}
        ></textarea>
      )}
      {withButton}
    </ScInput>
  );
};
