import styled, { keyframes } from "styled-components/macro";
import { theme } from "../../theme";

const animation = keyframes`
  0%{
    opacity: 0;
  }
  25%{
    opacity: 1;
  }
  50%{
    opacity: 1;
  }
  75%{
    opacity: 1;
  }
  100%{
    opacity: 0;
  }
`;

export const ScTextParser = styled.div`
  background-color: ${theme.console.black};
  height: 100vh;
  max-height: calc(100vh - 188px);
  overflow-y: auto;
  font-size: 12px;
  color: ${theme.color.white};
  font-family: ${theme.font.roboto};
  padding: 12px 16px 16px;
  border-radius: 0 0 8px 8px;
  p {
    margin-bottom: 16px;

    &:last-child {
      margin-bottom: 0;
    }

    &.comment {
      color: ${theme.console.green};
      opacity: 0.6;
    }
  }

  a {
    text-decoration: none;
    white-space: nowrap;
    overflow: hidden;
    display: block;
    text-overflow: ellipsis;
    color: ${theme.console.green};
    margin: 8px 0;
  }

  .animation {
    opacity: 0;
    animation: ${animation} 1s linear infinite alternate;
  }
`;
