import React, { FC } from "react";
import { ScTextParser } from "./styled";
import { DEFAULT_COMMENT_MESSAGE } from "actions/showBody";

interface ITextParserProps {
  text?: string;
}

export const TextParser: FC<ITextParserProps> = ({ text }) => {
  const paragraphs = text?.split("\n");

  console.log(paragraphs);

  return (
    <ScTextParser>
      {paragraphs?.map((paragraph, index) => {
        const links = paragraph.match(/(https?:\/\/[^\s]+)/g);
        const comments = paragraph.match(/<!--[\s\S]*?-->/g);

        if (links || comments) {
          const splitParagraph = paragraph.split(
            /(https?:\/\/[^\s]+|<!--[\s\S]*?-->)/g
          );
          return (
            <p key={index}>
              {splitParagraph.map((segment, segmentIndex) => {
                if (links && links.includes(segment)) {
                  const cleanedLink = segment.replace(/[\W_]+$/g, "");
                  return (
                    <a href={cleanedLink} target="_blank" key={segmentIndex}>
                      {segment}
                    </a>
                  );
                } else if (comments?.includes(segment)) {
                  return (
                    <p className="comment" key={segmentIndex}>
                      {segment}
                    </p>
                  );
                } else {
                  return segment;
                }
              })}
            </p>
          );
        }

        return (
          <p
            key={index}
            className={
              (paragraph === DEFAULT_COMMENT_MESSAGE && "comment") || ""
            }
          >
            {paragraph}
          </p>
        );
      })}
      <span className="animation">|</span>
    </ScTextParser>
  );
};
