import React, { useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch, ThunkDispatch } from "@reduxjs/toolkit";

import { RootState } from "./store";

import { Layout } from "./modules/Layout";
import { Board } from "./modules/Board/components";

import { ShowBodyAction } from "./actions/showBody";
import { Loader } from "components/Loader";
import { PagePlug } from "modules/Plug/components";

import GlobalStyle from "./styles";

function App() {
  const bodyDispatch: ThunkDispatch<RootState, any, ShowBodyAction> =
    useDispatch<Dispatch>();

  const issues = useSelector((state: RootState) => state.issues);
  const body = useSelector((state: RootState) => state.body);

  const isFirstStart = useMemo(
    () => issues.data.length === 0 && issues.error === null && !issues.loading,
    [issues]
  );

  return (
    <>
      <GlobalStyle />
      <Layout body={body} dispatch={bodyDispatch}>
        {isFirstStart || issues.error !== null ? (
          <PagePlug />
        ) : issues.loading ? (
          <Loader />
        ) : (
          <Board body={body} />
        )}
      </Layout>
    </>
  );
}

export default App;
