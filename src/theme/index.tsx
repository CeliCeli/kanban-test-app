export const theme = {
  color: {
    primary: "#313E45",
    secondary: "#5A786F",
    secondaryLight: "#E9E9E9",
    tertiary: "#CCD2C6",
    accent: "#21D07C",
    white: "#FFFFFF",
  },
  console: {
    black: "#232332",
    primary: "#302F45",
    secondary: "#4885FF",
    green: "#00C89C",
    orange: "#FF8359",
    red: "#FB3728",
    white: "#FFFFFF",
  },
  font: {
    roboto: `${"'Roboto Mono', monospace"}`,
    work: `${"'Work Sans', sans-serif"}`,
  },
  effect: {
    cardShadow: "0px 23px 40px rgba(120, 146, 130, 0.1)",
  },
};
