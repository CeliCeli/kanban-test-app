import { createGlobalStyle } from "styled-components/macro";
import { theme } from "../theme";

const GlobalStyle = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300&family=Work+Sans:wght@400;700&display=swap');

    body {
        margin: 0;
        padding: 0;
        font-family: ${theme.font.work};
        color: ${theme.color.white};
        background-color: ${theme.console.black};
    } 

    img{
        max-width: 100%;
        vertical-align: bottom;
    }

    h1,h2,h3,h4,h5{
        margin: 0;
        padding: 0;
    }

    h1{
        font-size: 28px;
        line-height: 120%;
    }

    h2{
        font-size: 24px;
        line-height: 140%;
    }

    p{
        margin: 0;
    }

    .drag-image{
        width: 10px;
        height: 10px;
        background-color: red;
    }

`;

export default GlobalStyle;
