export enum BoardTitles {
  Backlog = "Backlog",
  InProcess = "In Process",
  Done = "Done",
}
