const gitHubApiUrl: string = "https://api.github.com/";

export const shortMonths: string[] = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export default { gitHubApiUrl, shortMonths };
