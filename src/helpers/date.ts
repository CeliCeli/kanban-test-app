import constants from "../constants";

export const getDate = (int: string): string => {
  const date = new Date(int);

  return `${constants.shortMonths[date.getMonth()]} ${date.getDate()}`;
};
