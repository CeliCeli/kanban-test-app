import { BoardTitles } from "constants/data";
import { Data, Issue } from "models/board";
import { v4 as uuid } from "uuid";
import { TValue } from "./boardHistory";

export const createData = (arr: Issue[], key: string): Data[] => {
  const localKey = localStorage.getItem(key);
  if (!localKey || JSON.parse(localKey).length === 0) {
    return [
      {
        title: BoardTitles.Backlog,
        id: uuid(),
        items: arr,
      },
      {
        title: BoardTitles.InProcess,
        id: uuid(),
        items: [],
      },
      {
        title: BoardTitles.Done,
        id: uuid(),
        items: [],
      },
    ];
  } else {
    const boardsNames = Object.values(BoardTitles);
    const idArray: TValue[] = JSON.parse(localKey);

    const sortedArr = idArray.reduce((acc: Issue[], { items }) => {
      const itemsInArr = arr.filter(({ id }) => items.includes(id));
      return [...acc, ...itemsInArr];
    }, []);

    const searchedIdArr = idArray.map(({ items }) => [...items]).flat();
    const currentIdArr = sortedArr.map(({ id }) => id);

    const newIssues: Issue[] = arr.filter(({ id }) =>
      currentIdArr
        .filter((value, index, self) => {
          return (
            self.indexOf(value) === index && !searchedIdArr.includes(value)
          );
        })
        .includes(id)
    );

    return boardsNames.map((el, index) => {
      const currId: number[] = idArray.filter((_, idx) => idx === index)[0]
        .items;
      const currIssues = sortedArr.filter((issue) => currId.includes(issue.id));
      return {
        title: el as BoardTitles,
        id: uuid(),
        items: index === 0 ? [...currIssues, ...newIssues] : currIssues,
      };
    });
  }
};
