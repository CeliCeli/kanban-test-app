import { Data } from "models/board";

export enum ActionTypes {
  Default,
  Smart,
}

export type TValue = { label: number; items: number[] };

export const boardHistory = (
  label: string,
  initValue: Data[]
): { array: TValue[] | null } => {
  const item = localStorage.getItem(label);

  const value: TValue[] = initValue.map(({ title, items }, index) => ({
    label: index,
    items: items.map(({ id }) => id),
  }));

  const JSONValue = JSON.stringify(value);

  const smartSaveData = () => {
    const action = () => localStorage.setItem(label, JSONValue);

    window.addEventListener("beforeunload", action);
    return () => window.removeEventListener("beforeunload", action);
  };

  smartSaveData();

  const array = item ? JSON.parse(item) : null;

  return { array };
};
