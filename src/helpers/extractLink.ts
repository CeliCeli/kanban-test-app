import { LabelState } from "reducers/labelReducer";

export const extractLink = (link: string): [string, string] => {
  const pattern = /github\.com\/([^/]+)\/([^/]+)($|\/)/i;
  const match = link.match(pattern);
  if (!match) {
    return ["", ""];
  }
  return [match[1], match[2]];
};

export const getKey = (value: LabelState): string => {
  if (!value.labels) return "EmptyKeyErrCode";
  return value.labels
    .map(({ key }) => key)
    .reduce((acc, curr) => `${acc}/${curr}`);
};
