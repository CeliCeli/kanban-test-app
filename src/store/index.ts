import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "../reducers";
import { bodyReducer } from "../reducers/showBody";
import labelReducer from "reducers/labelReducer";

const rootReducer = combineReducers({
  issues: reducer,
  body: bodyReducer,
  label: labelReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
