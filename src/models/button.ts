import { ReactNode } from "react";

export enum ButtonTheme {
  Primary = "primary",
  Secondary = "secondary",
}

export interface ScButtonProps {
  color?: ButtonTheme;
}

export interface ButtonProps extends ScButtonProps {
  children: ReactNode;
  onClick?: (e: React.MouseEvent<HTMLElement>) => void;
  className?: string;
  id?: string;
  disabled?: boolean;
}
