export type Gen = {
  id: number | string;
  title: string;
};

export type Item = Issue;

export interface BoardItemProps extends Item {}

export interface Data extends Gen {
  items: Issue[];
}

export interface DragProps {
  onDragOver: (e: React.DragEvent) => void;
  onDragLeave: (e: React.DragEvent) => void;
  onDragStart: (e: React.DragEvent) => void;
  onDragEnd: (e: React.DragEvent) => void;
  onDrop: (e: React.DragEvent) => void;
}

export type IssueUser = {
  avatar_url: string;
  login: string;
  url: string;
  html_url: string;
};

export interface Issue {
  state: string;
  id: number;
  number: number;
  title: string;
  updated_at: string;
  repository_url: string;
  url: string;
  user: IssueUser;
  body: string;
}
