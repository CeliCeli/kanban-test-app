export enum InputTypes {
  Text = "text",
  Number = "number",
  File = "file",
  Textarea = "textarea",
}
export interface ScInputProps {
  withIcon?: JSX.Element;
  withButton?: JSX.Element;
  isFilled?: boolean;
  isError?: boolean;
  width?: number;
  height?: number;
  type?: string;
}
export interface InputProps extends ScInputProps {
  label?: string;
  placeholder?: string;
  value?: string | number;
  id?: string;
  readonly?: boolean;
  dataKey?: string;
  onChange?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onKeyUp?: (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onKeyDown?: (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onBlur?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onFocusCapture?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onFocus?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  className?: string;
}
