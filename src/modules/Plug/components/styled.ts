import { hexToRGB } from "helpers/hexToRgb";
import styled from "styled-components/macro";
import { theme } from "theme";

export const ScPagePlug = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 0 0 100%;
  max-width: 100%;
  padding: 47px 16px 0 16px;
  box-sizing: border-box;
  gap: 16px;
  position: relative;

  .emptyRow {
    flex: 0 0 calc(25% - 16px);
    max-width: calc(25% - 16px);
    box-sizing: border-box;
    border: 2px dashed ${hexToRGB(theme.color.white, 0.1)};
    stroke-dashoffset: 20px;
    height: calc(100% - 40px);
    max-height: calc(100% - 40px);
    border-radius: 16px;
  }

  .message {
    min-height: 128px;
    padding-bottom: 24px;
    width: 380px;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    border-radius: 16px;
    border: 2px dashed ${hexToRGB(theme.color.white, 0.1)};
    background-color: ${theme.console.black};

    .headline {
      display: flex;
      justify-content: flex-end;
      padding: 8px 16px;
      span {
        width: 12px;
        height: 12px;
        border-radius: 50%;
        background-color: ${hexToRGB(theme.color.white, 0.1)};
        margin-left: 8px;
      }
    }
    .text {
      text-align: center;
      h2 {
        margin-bottom: 16px;
      }
      p {
        line-height: 1.6em;
        margin-bottom: 16px;
      }
    }
  }
`;
