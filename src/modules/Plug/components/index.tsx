import { FC, useEffect } from "react";
import { ScPagePlug } from "./styled";
import { Button } from "components/Button";
import { IssuesActionTypes, fetchIssues } from "actions";
import { Dispatch, ThunkDispatch } from "@reduxjs/toolkit";
import { RootState } from "store";
import { useDispatch, useSelector } from "react-redux";
import { extractLink } from "helpers/extractLink";
import { LabelActionTypes, addLabel } from "actions/labelAction";

export const PagePlug: FC = () => {
  const dispatch: ThunkDispatch<RootState, any, IssuesActionTypes> =
    useDispatch<Dispatch>();

  const labelDispatch: ThunkDispatch<RootState, any, LabelActionTypes> =
    useDispatch<Dispatch>();

  const { error } = useSelector((state: RootState) => state.issues);

  const handleClick = () => {
    dispatch(
      fetchIssues(extractLink("https://github.com/facebook/react"), {
        key: "facebook/react",
        arr: [],
      })
    );
    labelDispatch(addLabel("https://github.com/facebook/react"));
  };

  useEffect(() => {
    if (error === null) return;
    labelDispatch(addLabel(null));
  }, [error]);

  return (
    <ScPagePlug>
      {Array(4)
        .fill(null)
        .map((_, i) => (
          <div className="emptyRow" key={i} />
        ))}
      <div className="message">
        <div className="headline">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div className="text">
          {error !== null ? (
            <>
              <h2>Oops!</h2>
              <p>
                There seems to be no such repo{` :(`}
                <br />
                Try using the moc link
              </p>

              <Button onClick={handleClick}>
                https://github.com/facebook/react
              </Button>
            </>
          ) : (
            <>
              <h2>Hi Newbie!</h2>
              <p>
                For starters <br /> enter the repo link, or use the moc link:
                <br />
              </p>
              <Button onClick={handleClick}>
                https://github.com/facebook/react
              </Button>
            </>
          )}
        </div>
      </div>
    </ScPagePlug>
  );
};
