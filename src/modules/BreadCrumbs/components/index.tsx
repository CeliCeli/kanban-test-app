import React, { FC } from "react";
import { hexToRGB } from "helpers/hexToRgb";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { theme } from "theme";

import styled from "styled-components";

export const BreadCrumbs: FC = () => {
  const { labels } = useSelector((state: RootState) => state.label);

  return labels.length !== 0 ? (
    <ScBreadCrumbs>
      {labels.map(({ key, link }, index) => (
        <li key={index}>
          <a href={link} target="_blank">
            {key}
          </a>
        </li>
      ))}
    </ScBreadCrumbs>
  ) : null;
};

const ScBreadCrumbs = styled.ul`
  display: flex;
  align-items: center;
  margin: 0;
  padding: 8px 16px;
  border-radius: 4px;
  background-color: ${theme.console.primary};
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);

  li {
    margin-left: 16px;
    list-style: none;
    position: relative;
    &:first-child {
      margin-left: 0;
    }
    &:not(:last-child) {
      padding-right: 16px;
      &:before {
        position: absolute;
        content: "|";
        right: 0;
        color: ${theme.color.white};
      }
      a {
      }
    }
  }

  a {
    display: block;
    text-decoration: none;
    color: ${theme.color.white};
    font-weight: 600;

    &:hover {
      color: ${hexToRGB(theme.console.green, 0.6)};
    }
  }
`;
