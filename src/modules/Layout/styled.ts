import styled, { css } from "styled-components/macro";
import { theme } from "../../theme";

export const ScHeader = styled.div`
  height: 64px;
  background-color: #14141f;
  display: flex;
  justify-content: space-between;
  padding: 0 16px 0 32px;
  align-items: center;
  .logo {
    color: ${theme.color.white};
    font-weight: 700;
    text-decoration: none;
    margin-right: 16px;
  }
`;
export const ScLayout = styled.div<{ widthBody?: boolean }>`
  display: flex;
  flex-wrap: nowrap;
  padding: 0 8px;
  min-height: calc(100vh - 64px);
  max-height: calc(100vh - 64px);
  justify-content: space-between;
  overflow: hidden;

  aside {
    flex: 0 0 25%;
    max-width: calc(25% - 16px);
    margin: 48px 16px 0 8px;
    padding: 0;
    box-sizing: border-box;
    right: -25%;
    color: #fff;
    word-wrap: break-word;
    overflow-wrap: break-word;
    opacity: 0;
    transition: 0.2s;
  }

  .comentConsole {
    border: 2px solid ${theme.console.primary};
    border-radius: 8px;

    .headline {
      font-size: 12px;
      font-family: ${theme.font.work};
      color: ${theme.color.white};
      background-color: ${theme.console.primary};
      display: flex;
      justify-content: space-between;
      padding: 4px 8px;
      align-items: center;
      border-radius: 8px 8px 0 0;
    }
    button {
      padding: 0;
      border: none;
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background-color: ${theme.console.red};
      cursor: pointer;
    }
  }

  ${({ widthBody }) =>
    widthBody &&
    css`
      aside {
        right: 16px;
        opacity: 1;
      }
    `}
`;
