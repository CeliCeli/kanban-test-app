import { FC, ReactNode, useMemo, useState } from "react";
import { connect, ConnectedProps, useDispatch, useSelector } from "react-redux";
import { Dispatch, ThunkDispatch } from "@reduxjs/toolkit";
import { RootState } from "store";
import { SHOW_BODY } from "actions/showBody";
import { TextParser } from "components/TextParser";
import { ScHeader, ScLayout } from "./styled";
import { Button } from "components/Button";
import { Input } from "components/Input";
import { IssuesActionTypes, fetchIssues } from "actions";
import { extractLink } from "helpers/extractLink";
import { LabelActionTypes, addLabel } from "actions/labelAction";
import { createData } from "helpers/createData";

type Props = ConnectedProps<typeof connector>;

const mapStateToProps = (state: RootState) => ({
  body: state.body,
});

const connector = connect(mapStateToProps);

export const Layout: FC<{ children?: ReactNode } & Props> = ({
  children,
  body,
}) => {
  const [searchValue, setSearchValue] = useState<string>("");
  const dispatch = useDispatch();
  const search: ThunkDispatch<RootState, any, IssuesActionTypes> =
    useDispatch<Dispatch>();

  const labelDispatch: ThunkDispatch<RootState, any, LabelActionTypes> =
    useDispatch<Dispatch>();

  const issues = useSelector((state: RootState) => state.issues);

  const handleClick = () => {
    dispatch({ type: SHOW_BODY, payload: { body: "" } });
  };

  const handleSearch = async () => {
    const [owner, repo] = extractLink(searchValue);
    const accData = createData(issues.data, `${owner}/${repo}`);
    labelDispatch(addLabel(searchValue));
    await search(
      fetchIssues([owner, repo], {
        key: `${owner}/${repo}`,
        arr: accData,
      })
    ).then(() => setSearchValue(""));
  };

  const widthBody: boolean = useMemo(() => body?.length > 0, [body]);

  return (
    <>
      <ScHeader>
        <a href="/" className="logo">
          kanban test app
        </a>
        <Input
          placeholder="https://github.com/facebook/react"
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
          withButton={
            <Button disabled={searchValue.length === 0} onClick={handleSearch}>
              Search
            </Button>
          }
        />
      </ScHeader>
      <ScLayout widthBody={widthBody}>
        {children}
        <aside>
          {widthBody && (
            <div className="comentConsole">
              <div className="headline">
                Comment:
                <button onClick={handleClick} />
              </div>
              <TextParser text={body} />
            </div>
          )}
        </aside>
      </ScLayout>
    </>
  );
};
