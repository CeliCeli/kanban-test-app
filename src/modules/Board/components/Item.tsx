import { FC } from "react";
import { useDispatch } from "react-redux";
import { ScBoardItem } from "./styled";
import { BoardItemProps, DragProps } from "../../../models/board";
import { getDate } from "../../../helpers/date";
import { SHOW_BODY } from "../../../actions/showBody";

export const BoardItem: FC<BoardItemProps & DragProps> = ({
  title,
  user,
  url,
  number: id,
  updated_at,
  body,
  onDragOver,
  onDragLeave,
  onDragStart,
  onDragEnd,
  onDrop,
}) => {
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch({ type: SHOW_BODY, payload: { body } });
  };
  const date = getDate(updated_at);

  return (
    <ScBoardItem
      onDragOver={onDragOver}
      onDragLeave={onDragLeave}
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      onDrop={onDrop}
      onClick={handleClick}
      draggable
      className="item"
    >
      <a href={url} target="_blank" className="headline">
        {`Task Id`} <span>#{id}</span>
      </a>
      <div className="title">{`/* ${title} */ `}</div>

      <div className="btmLine">
        <div className="imgWrp">
          <img src={user.avatar_url} />
          <a href={user.html_url} target="_blank">
            {user.login}
          </a>
        </div>
        <span className="date">// {date}</span>
      </div>
    </ScBoardItem>
  );
};
