import styled, { css } from "styled-components/macro";
import { theme } from "theme";
import { hexToRGB } from "helpers/hexToRgb";

export const ScBoardContainer = styled.div<{ withBody?: boolean }>`
  flex: 0 0 33.3%;
  max-width: 33.3%;
  box-sizing: border-box;
  padding: 16px 8px;
  transition: 0.2s;

  *::selection {
    background-color: ${theme.color.secondary};
    color: ${theme.color.white};
  }

  h3 {
    padding-left: 16px;
    margin-bottom: 16px;
    font-size: 14px;
    cursor: default;
    position: relative;

    display: flex;
    justify-content: space-between;
    align-items: center;

    span {
      opacity: 0.2;
    }

    button {
      position: absolute;
      right: 0;
      top: 50%;
      transform: translateY(-50%);
    }

    &::selection {
      background-color: transparent;
      color: ${theme.color.white};
    }
  }

  ${({ withBody }) =>
    withBody &&
    css`
      flex: 0 0 25%;
      max-width: 25%;
    `}
`;
export const ScBoard = styled.div`
  height: calc(100% - 40px);
  max-height: calc(100% - 40px);
  overflow-y: auto;
  box-sizing: border-box;
  background-color: ${theme.console.primary};
  border-radius: 8px;
  padding: 12px;
`;

export const ScBoardItem = styled.div`
  font-family: ${theme.font.roboto};
  margin-bottom: 12px;
  border-radius: 10px;
  box-sizing: border-box;
  transition: 0.2s;
  cursor: pointer;
  position: relative;
  background-color: ${theme.console.black};
  border: 2px solid ${theme.console.black};
  color: ${theme.color.white};
  padding-bottom: 16px;
  .headline {
    font-size: 12px;
    background-color: ${theme.console.primary};
    color: ${theme.color.white};
    text-decoration: none;
    font-style: italic;
    display: flex;
    justify-content: space-between;
    padding: 4px 8px;
    border-radius: 8px 8px 0 0;

    &:hover {
      color: ${theme.console.green};
    }

    span {
      opacity: 0.4;
    }
  }

  .title {
    padding: 16px;
    font-size: 14px;
    position: relative;
    color: ${theme.console.green};
    opacity: 0.7;
  }

  .btmLine {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 16px;
  }

  .imgWrp {
    display: flex;
    align-items: center;

    img {
      flex: 0 0 32px;
      max-width: 32px;
      height: 32px;
      object-fit: cover;
      margin-right: 8px;
      border: 2px solid ${theme.color.primary};
      border-radius: 50%;
    }

    a {
      color: ${theme.color.white};
      text-decoration: none;
      font-size: 14px;

      &:hover {
        color: ${theme.console.green};
      }
    }
  }

  .date {
    font-size: 14px;
    opacity: 0.4;
  }

  &:last-child {
    margin-bottom: 0;
  }

  &:before {
    position: absolute;
    content: "";
    opacity: 0;
    left: 0;
    right: 0;
    bottom: -12px;
    height: 4px;
    border-radius: 4px;
    background-color: ${theme.color.tertiary};
    transition: 0.2s;
  }

  &.highlight {
    margin-bottom: 20px;
    &:before {
      opacity: 1;
    }
  }
`;

export const ScPlug = styled.div<{ isHover: boolean }>`
  background-color: ${hexToRGB(theme.color.white, 0.02)};
  height: 128px;
  border-radius: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: ${hexToRGB(theme.console.white, 0.1)};
  pointer-events: none;
  transition: height 0.2s;
  position: relative;

  &::before {
    position: absolute;
    content: "Drop issue here!";
    color: transparent;
  }

  b {
    margin: 0 4px;
  }
  ${({ isHover }) =>
    isHover &&
    css`
      height: 100%;
      color: transparent;
      &:before {
        color: ${hexToRGB(theme.console.white, 0.1)};
      }
    `}
`;
