import { FC, useEffect, useMemo, useState } from "react";
import { ScBoardContainer, ScBoard, ScPlug } from "./styled";
import { BoardItem } from "./Item";
import { Data, Item } from "models/board";
import { Button } from "components/Button";
import { useDispatch, useSelector } from "react-redux";
import { DEFAULT_COMMENT_MESSAGE, SHOW_BODY } from "actions/showBody";
import { boardHistory } from "helpers/boardHistory";
import { RootState } from "store";
import { createData } from "helpers/createData";
import { getKey } from "helpers/extractLink";
import { BreadCrumbs } from "modules/BreadCrumbs/components";

export const Board: FC<{ body: string }> = ({ body }) => {
  const dispatch = useDispatch();

  const [data, setData] = useState<Data[]>([]);
  const [currentBoard, setCurrentBoard] = useState<Data | null>(null);
  const [currentItem, setCurrentItem] = useState<Item | null>(null);
  const [hoverIndex, setHoverIndex] = useState<number>(-1);

  const {
    error,
    loading,
    data: issueData,
  } = useSelector((state: RootState) => state.issues);
  const breadCrumbs = useSelector((state: RootState) => state.label);

  const handleClick = () => {
    dispatch({
      type: SHOW_BODY,
      payload: { body: DEFAULT_COMMENT_MESSAGE },
    });
  };

  const dragOverHandler = (e: React.DragEvent, board: Data) => {
    e.preventDefault();
    const target = e.target as HTMLElement;
    if (target.classList.contains("item")) {
      target.classList.add("highlight");
    }
    if (board.items.length === 0) {
      const index = data.indexOf(board);
      setHoverIndex(index);
    }
  };
  const dragLeaveHandler = (e: React.DragEvent) => {
    const target = e.target as HTMLElement;
    target.classList.remove("highlight");
    setHoverIndex(-1);
  };
  const dragStartHandler = (e: React.DragEvent, board: Data, item: Item) => {
    setCurrentBoard(board);
    setCurrentItem(item);
  };

  const dragEndHandler = (e: React.DragEvent) => {
    const target = e.target as HTMLElement;
    target.classList.remove("highlight");
    setHoverIndex(-1);
  };
  const dropHandler = (e: React.DragEvent, board: Data, item: Item) => {
    e.preventDefault();
    const target = e.target as HTMLElement;
    target.classList.remove("highlight");
    setHoverIndex(-1);

    if (!currentItem || !currentBoard) return;

    const currentIndex = currentBoard.items.indexOf(currentItem);
    currentBoard.items.splice(currentIndex, 1);

    const dropIndex = board.items.indexOf(item);
    board.items.splice(dropIndex, 0, currentItem);

    const newData: Data[] = data.map((el) => {
      if (el.id === board.id) {
        return board;
      }
      if (el.id === currentBoard.id) {
        return currentBoard;
      }
      return el;
    });

    setData(newData);
  };
  const dropCardHandler = (e: React.DragEvent, board: Data) => {
    e.preventDefault();

    if (!currentItem || !currentBoard || board.items.length != 0) return;
    board.items.push(currentItem);
    const currentIndex = currentBoard.items.indexOf(currentItem);
    currentBoard.items.splice(currentIndex, 1);

    const newData: Data[] = data.map((el) => {
      if (el.id === board.id) {
        return board;
      }
      if (el.id === currentBoard.id) {
        return currentBoard;
      }
      return el;
    });

    setData(newData);
  };
  const showButton: boolean = useMemo(
    () => body !== DEFAULT_COMMENT_MESSAGE && body?.length === 0,
    [body]
  );

  useEffect(() => {
    if (!error && !loading) {
      const initData = createData(issueData, getKey(breadCrumbs));
      setData(initData);
    }
  }, [issueData, error, loading]);

  useEffect(() => {
    if (data.length === 0 || breadCrumbs.labels.length === 0) return;
    boardHistory(getKey(breadCrumbs), data);
  }, [data]);

  return (
    <>
      {data.map((board, index) => (
        <ScBoardContainer withBody={body?.length > 0} key={index}>
          <h3>
            <span> {board.title}</span>
            {index === 0 && <BreadCrumbs />}
            {index === data?.length - 1 && showButton && (
              <Button onClick={handleClick}>Show comments</Button>
            )}
          </h3>
          <ScBoard
            onDrop={(e) => dropCardHandler(e, board)}
            onDragOver={(e) => dragOverHandler(e, board)}
          >
            {board.items.length !== 0 ? (
              board.items.map((item, i) => (
                <BoardItem
                  onDragOver={(e) => dragOverHandler(e, board)}
                  onDragLeave={dragLeaveHandler}
                  onDragStart={(e) => dragStartHandler(e, board, item)}
                  onDragEnd={dragEndHandler}
                  onDrop={(e) => dropHandler(e, board, item)}
                  {...item}
                  key={i}
                />
              ))
            ) : (
              <ScPlug isHover={hoverIndex === index}>
                The <b>{board.title}</b> board is empty
              </ScPlug>
            )}
          </ScBoard>
        </ScBoardContainer>
      ))}
    </>
  );
};
