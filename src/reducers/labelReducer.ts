import { ADD_LABEL, LabelActionTypes } from "actions/labelAction";

export interface LabelState {
  labels: {
    key: string;
    link: string;
  }[];
}

const initialState: LabelState = {
  labels: [],
};

const labelReducer = (
  state = initialState,
  action: LabelActionTypes
): LabelState => {
  switch (action.type) {
    case ADD_LABEL:
      return { ...state, labels: action.payload };
    default:
      return state;
  }
};

export default labelReducer;
