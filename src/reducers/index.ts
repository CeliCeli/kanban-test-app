import {
  FETCH_ISSUES_FAILURE,
  FETCH_ISSUES_REQUEST,
  FETCH_ISSUES_SUCCESS,
  IssuesActionTypes,
} from "../actions";
import { Issue } from "../models/board";

interface IssuesState {
  data: Issue[];
  loading: boolean;
  error: string | null;
}

const initialState: IssuesState = {
  data: [],
  loading: false,
  error: null,
};

const reducer = (
  state = initialState,
  action: IssuesActionTypes
): IssuesState => {
  switch (action.type) {
    case FETCH_ISSUES_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_ISSUES_SUCCESS:
      const { payload } = action;
      const data = payload.map(
        ({
          state,
          id,
          title,
          updated_at,
          repository_url,
          user,
          body,
          url,
          number,
        }) => ({
          id,
          title,
          repository_url: repository_url
            .replace("/api.", "")
            .replace("/repos/", "/"),
          state,
          number,
          body,
          url: url.replace("/api.", "").replace("/repos/", "/"),
          user: {
            avatar_url: user.avatar_url,
            login: user.login,
            url: user.url,
            html_url: user.html_url,
          },
          updated_at,
        })
      );
      return {
        ...state,
        data,
        loading: false,
        error: null,
      };
    case FETCH_ISSUES_FAILURE:
      const { payload: error } = action;
      return {
        ...state,
        loading: false,
        error,
      };
    default:
      return state;
  }
};
export default reducer;
