import { SHOW_BODY, ShowBodyAction } from "../actions/showBody";

export const bodyReducer = (state = "", action: ShowBodyAction): string => {
  switch (action.type) {
    case SHOW_BODY:
      return action.payload.body;
    default:
      return state;
  }
};
