import { extractLink } from "helpers/extractLink";

export const ADD_LABEL = "ADD_LABEL";

export interface Label {
  key: string;
  link: string;
}

interface AddLabelAction {
  type: typeof ADD_LABEL;
  payload: Label[];
}

export type LabelActionTypes = AddLabelAction;

export const addLabel = (link: string | null): AddLabelAction => {
  if (link === null) return { type: ADD_LABEL, payload: [] };

  const [owner, repo] = extractLink(link);

  const labelData = [
    { key: owner, link: `https://github.com/${owner}` },
    { key: repo, link },
  ];

  return { type: ADD_LABEL, payload: labelData };
};
