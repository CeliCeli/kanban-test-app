import { Dispatch } from "redux";
import axios from "axios";

import constants from "../constants";
import { Data, Issue } from "../models/board";
import { boardHistory } from "helpers/boardHistory";

export const FETCH_ISSUES_REQUEST = "FETCH_ISSUES_REQUEST";
export const FETCH_ISSUES_SUCCESS = "FETCH_ISSUES_SUCCESS";
export const FETCH_ISSUES_FAILURE = "FETCH_ISSUES_FAILURE";

interface FetchIssuesRequestAction {
  type: typeof FETCH_ISSUES_REQUEST;
}

interface FetchIssuesSuccessAction {
  type: typeof FETCH_ISSUES_SUCCESS;
  payload: Issue[];
}

interface FetchIssuesFailureAction {
  type: typeof FETCH_ISSUES_FAILURE;
  payload: string;
}

export type IssuesActionTypes =
  | FetchIssuesRequestAction
  | FetchIssuesSuccessAction
  | FetchIssuesFailureAction;

export const fetchIssuesRequest = (): IssuesActionTypes => ({
  type: FETCH_ISSUES_REQUEST,
});

export const fetchIssuesSuccess = (issues: Issue[]): IssuesActionTypes => ({
  type: FETCH_ISSUES_SUCCESS,
  payload: issues,
});

export const fetchIssuesFailure = (error: string): IssuesActionTypes => ({
  type: FETCH_ISSUES_FAILURE,
  payload: error,
});

export const fetchIssues =
  ([owner, repo]: string[], accData: { arr: Data[] | []; key: string }) =>
  async (dispatch: Dispatch<IssuesActionTypes>) => {
    dispatch(fetchIssuesRequest());
    if (owner.length === 0 || repo.length === 0) {
      fetchIssuesFailure("error");
    }
    return axios
      .get<Issue[]>(`${constants.gitHubApiUrl}repos/${owner}/${repo}/issues`)
      .then((response) => {
        if (accData.arr.length !== 0) {
          boardHistory(accData.key, accData.arr);
        }
        return dispatch(fetchIssuesSuccess(response.data));
      })
      .catch((error) => dispatch(fetchIssuesFailure(error.message)));
  };
