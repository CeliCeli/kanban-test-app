import { Action } from "redux";

export const SHOW_BODY = "SHOW_BODY";
export const DEFAULT_COMMENT_MESSAGE = "// Select a task to view comments ";

export interface ShowBodyAction extends Action<typeof SHOW_BODY> {
  payload: {
    body: string;
  };
}

export const showBody = (body: string): ShowBodyAction => ({
  type: SHOW_BODY,
  payload: {
    body,
  },
});
